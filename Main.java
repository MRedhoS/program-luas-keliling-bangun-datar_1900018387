package com.redho;

import java.util.Scanner;

public class Main {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        persegi pg = new persegi();
        persegiPanjang pp = new persegiPanjang();
        balok bl = new balok();
        kubus kb = new kubus();

        System.out.println("Pilih Jenis Bangun Datar/Ruang");
        System.out.println("1. Persegi");
        System.out.println("2. Persegi Panjang");
        System.out.print("Masukkan Pilihan : ");
        int pil1 = input.nextInt();
        System.out.println(" ");

        switch(pil1){
            case 1:
                System.out.print("Masukkan Sisi : ");
                pg.s = input.nextDouble();
                System.out.println(" ");

                System.out.println("1. Cari Luas");
                System.out.println("2. Cari Keliling");
                System.out.print("Masukkan Pilihan : ");
                int pil2 = input.nextInt();

                if (pil2 == 1){
                    System.out.println("Luas Persegi adalah : " + pg.luas());
                }else if (pil2 == 2){
                    System.out.println("Keliling Persegi adalah : " + pg.keliling());
                }
                break;
            case 2:
                System.out.print("Masukkan Panjang : ");
                pp.p = input.nextDouble();
                System.out.print("Masukkan Lebar : ");
                pp.l = input.nextDouble();
                System.out.println(" ");

                System.out.println("1. Cari Luas");
                System.out.println("2. Cari Keliling");
                System.out.print("Masukkan Pilihan : ");
                int pil3 = input.nextInt();

                if (pil3 == 1){
                    System.out.println("Luas Persegi Panjang adalah : " + pp.luas());
                }else if (pil3 == 2){
                    System.out.println("Keliling Persegi Panjang adalah : " + pp.keliling());
                }
                break;

            default:
                System.out.println("INPUT ANGKA YANG BENER");
                break;

        }

    }
}